let ctx = null;
let isMouseDown = false;
let xBlock = null;
let yBlock = null;
let pi = Math.PI;
let colorValue = 'red';
let thickValue = '10';
const socket = new WebSocket("ws://localhost:3006");


socket.onopen = (event) => {
    console.log("WebSocket is opened now.");
    socket.send(JSON.stringify({controller: "SEND_PICTURE_NEW_CLIENT"}))
};

socket.onclose = (event) => {
    console.log("WebSocket is closed now.");
};

socket.onerror = (event) => {
    console.error("WebSocket error observed:", event);
};

socket.onmessage = (event) => {
    let response = JSON.parse(event.data);
    if(response.controller === "SEND_PICTURE"){
        for(let i = 0; i < response.array.length; i++){
            let data = JSON.parse(response.array[i]);
            if(data.isMouseDown === true) {
                draw(data.x, data.y, data.width, data.color)
            }else {
                ctx.beginPath();
            }
        }
    }else if(response.controller === "CLEAR_HOLST"){
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, 600, 400);
        ctx.beginPath();
    }else if(response.controller === "SEND_COORDINATION"){
        let finalResponse = JSON.parse(response.request);
        if(finalResponse.isMouseDown === true){
            draw(finalResponse.x, finalResponse.y, finalResponse.width, finalResponse.color);
        }else{
            ctx.beginPath();
        }
    }
};

export const init = (canvas) => {
    ctx = canvas.getContext('2d');
};

export const mouseDownCanvas = () => {
    isMouseDown = true;
};

export const mouseUpCanvas = () => {
    if(isMouseDown){
        let bodyCanvas = {
            x: null,
            y: null,
            width: thickValue,
            color: colorValue,
            isMouseDown: false,
            controller: 'SEND_COORDINATES'
        };
        socket.send(JSON.stringify(bodyCanvas));
    }
    isMouseDown = false;
    ctx.beginPath();
};

export const mouseMoveCanvas = (e) => {
    xBlock = document.querySelector('.xBlock');
    yBlock = document.querySelector('.yBlock');
    xBlock.innerHTML = e.clientX.toString();
    yBlock.innerHTML = e.clientY.toString();


    let bodyCanvas = {
        x: e.clientX,
        y: e.clientY,
        width: thickValue,
        color: colorValue,
        isMouseDown,
        controller: 'SEND_COORDINATES'
    };
    if(isMouseDown){
        socket.send(JSON.stringify(bodyCanvas));
    }
};

const draw = (x, y, lineThink, color) => {
    ctx.strokeStyle = color;
    ctx.lineTo(x, y);
    ctx.lineWidth = lineThink;
    ctx.stroke();

    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.arc(x, y, lineThink/2, 0, pi * 2);
    ctx.fill();

    ctx.beginPath();
    ctx.moveTo(x, y);
};

export const clean = () => {
    socket.send(JSON.stringify({controller: "CLEAN_PICTURE"}));
};


export const setColor = (event) => {
    switch(event.target.classList[0]){
        case 'color-red': return colorValue = 'red';
        case 'color-green': return colorValue = 'green';
        case 'color-blue': return colorValue = 'blue';
        default: return colorValue = 'red';
    }
};

export const rangeWidth = event => {
    socket.send(JSON.stringify({controller: "SET_RANGE", range: event.target.value}));
    let result = event.target.value / 10;
    thickValue = result.toString();
};
