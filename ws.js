const WebSocket = require("ws");
let array = [];

const server = new WebSocket.Server({port: 3006}, () => {
    console.log("ws was started on port 3006")
});

server.on('connection', ws => {
    ws.on("message", request => {
        let sendData = JSON.parse(request);
        switch(sendData.controller){
            case "SEND_COORDINATES": return sendPictureAllClients(request);
            case "SEND_PICTURE_NEW_CLIENT": return sendPictureNewClient();
            case "CLEAN_PICTURE": return cleanPicture();
            default: return null;
        }
    });
});

const sendPictureAllClients = (request) => {
    array.push(request);
    server.clients.forEach((client) => {
        if (client.readyState === WebSocket.OPEN) {
            client.send(JSON.stringify({controller: "SEND_COORDINATION", request}));
        }
    });
};

const sendPictureNewClient = () => {
    server.clients.forEach((client) => {
        if (client.readyState === WebSocket.OPEN) {
            client.send(JSON.stringify({controller: "SEND_PICTURE", array}));
        }
    });
};

const cleanPicture = () => {
    server.clients.forEach((client) => {
        if (client.readyState === WebSocket.OPEN) {
            array.length = 0;
            client.send(JSON.stringify({controller: "CLEAR_HOLST"}));
        }
    });
};
